// (1) import uses system.js polyfill
import {Component, View, CORE_DIRECTIVES} from 'angular2/angular2';

import {NamesList} from '../../services/NameList';
import {Home} from '../home/home';
// (2) a Component/View annotation refers to the class down
@Component({
  selector: 'about',     // (3) selector for component name. [about] - attribute .about - class.
                         // see https://github.com/angular/angular/blob/master/modules/angular2/docs/core/02_directives.md#css-selectors for more

  bindings: [Home],       // (4) to inject other modules, use [bindings]. Home neeeds to be declared as 'export'
  properties: ['xAbout'] // (5) this makes a *propery* (like attribute, but a member) on the object.
                          // now we can interpolate it

})
@View({
  templateUrl: './components/about/about.html', // (6) template url or template
  directives: [CORE_DIRECTIVES] // (7) we need to specify directives used by the view.
                                // directives are components without a view. eg ngFor, ngIf, etc.
})

export class About {
   xAbout:string = null;
   myhome:Home;
   // (8) Namelist, home is injected by the injector
   // public is a convinience for setting list as a public var
  constructor(public list: NamesList,home:Home) {
    this.xAbout = home.homeAbout;
    this.myhome = home;

  }
  addName(newname):boolean {
    this.list.add(newname.value);
    newname.value = '';
    // prevent default form submit behavior to refresh the page
    return false;
  }
}
