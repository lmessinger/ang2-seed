import {Component, View} from 'angular2/angular2';

@Component({
  selector: 'home'
})
@View({
  templateUrl: './components/home/home.html'
})
export class Home {
  // (13) uncomment to see binding in action
  // homeAbout:string = "home about";
  constructor() {
    // console.log('home about -',this.homeAbout);
    // // save for timeout
    // var that = this;
    // setTimeout(function() {
    //   // this here is a window
    //   that.homeAbout += " second later";
    //   console.log('home about - ',that,that.homeAbout);
    // },1000);
  }
}
