// (13) service is a simple class
// no annotations
// this by the way might lead to a problem if u want to inject into it
// http://blog.thoughtram.io/angular/2015/09/17/resolve-service-dependencies-in-angular-2.html

export class NamesList {
  names = ['Dijkstra', 'Knuth', 'Turing', 'Hopper'];

  get():string[] {
    return this.names;
  }
  add(value: string):void {
    this.names.push(value);
  }
}
